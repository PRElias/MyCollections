﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyCollections.Models
{
    public class GameApiDTO
    {
        public string Name { get; set; }
        public string Cover { get; set; }
    }
}
